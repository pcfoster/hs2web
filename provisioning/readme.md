#Acquia Cloud VM

##Usage

* Add to your project in the root as provisioning  
`git clone XXXXX provisioning`

* Change configuration in ansible/vars/common.yml for your project

* Make sure you have vagrant/virtualbox/ansible installed and run
`vagrant up`