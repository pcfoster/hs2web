<?php
/**
 * @file
 * hs2_blog.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function hs2_blog_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'body'
//removed

  // Exported field_base: 'field_author'
  $field_bases['field_author'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_author',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'field' => 'field_sort_name:value',
          'type' => 'field',
        ),
        'target_bundles' => array(
          'employee' => 'employee',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_subject_area'
  $field_bases['field_subject_area'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_subject_area',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'user_experience' => 'User Experience',
        'drupal' => 'Drupal',
        'interactive_marketing' => 'Interactive Marketing',
        'social_marketing' => 'Social Media',
        'interaction_design' => 'Interaction Design',
        'internet_technologies' => 'Internet Technologies',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_subjects'
  $field_bases['field_subjects'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_subjects',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'responsive_web_design' => 'Responsive web design',
        'reputation_management' => 'Reputation management',
        'local_listing_management' => 'Local listing management',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_summary'
  $field_bases['field_summary'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_summary',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 500,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
