<?php
/**
 * @file
 * hs2_employees.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function hs2_employees_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'employee_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Employee List';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Team';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'user-[nid] employee active';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
    'field_employee_group' => 'field_employee_group',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Alt Photo */
  $handler->display->display_options['fields']['field_alt_photo']['id'] = 'field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['table'] = 'field_data_field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['field'] = 'field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['label'] = '';
  $handler->display->display_options['fields']['field_alt_photo']['alter']['text'] = '<div class="employee-list-image" style="width:85px;height:85px;background-image:url(\'[field_alt_photo]\');"> </div>';
  $handler->display->display_options['fields']['field_alt_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_alt_photo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_alt_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_alt_photo']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_alt_photo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Job Title */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['label'] = '';
  $handler->display->display_options['fields']['field_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_title']['element_label_colon'] = FALSE;
  /* Field: Content: A few favorite things */
  $handler->display->display_options['fields']['field_favorite_things']['id'] = 'field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['table'] = 'field_data_field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['field'] = 'field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['label'] = 'A Few of My Favorite Things';
  $handler->display->display_options['fields']['field_favorite_things']['exclude'] = TRUE;
  /* Field: Content: Book love */
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['id'] = 'field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['table'] = 'field_data_field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['field'] = 'field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['label'] = 'Book I Can\'t Live Without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['exclude'] = TRUE;
  /* Field: Content: Childhood aspiration */
  $handler->display->display_options['fields']['field_childhood_career_goal']['id'] = 'field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['table'] = 'field_data_field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['field'] = 'field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['label'] = 'Childhood Career Goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['exclude'] = TRUE;
  /* Field: Content: Favorite at-work moment */
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['id'] = 'field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['table'] = 'field_data_field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['field'] = 'field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['label'] = 'Favorite Moment At Work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['exclude'] = TRUE;
  /* Field: Content: Inspired quote */
  $handler->display->display_options['fields']['field_favorite_quote']['id'] = 'field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['table'] = 'field_data_field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['field'] = 'field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['label'] = 'Favorite Quote';
  $handler->display->display_options['fields']['field_favorite_quote']['exclude'] = TRUE;
  /* Field: Content: Choice vacation */
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['id'] = 'field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['table'] = 'field_data_field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['field'] = 'field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['label'] = 'Favorite Vacation Spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['exclude'] = TRUE;
  /* Field: Content: Surprise factor */
  $handler->display->display_options['fields']['field_fun_fact']['id'] = 'field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['table'] = 'field_data_field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['field'] = 'field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['label'] = 'Fun Fact About Me That Always Surprises People';
  $handler->display->display_options['fields']['field_fun_fact']['exclude'] = TRUE;
  /* Field: Content: Song addiction */
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['id'] = 'field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['table'] = 'field_data_field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['field'] = 'field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['label'] = 'Most Listened-to Song on My iPod';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['exclude'] = TRUE;
  /* Field: Content: Dream-about meal */
  $handler->display->display_options['fields']['field_my_dream_about_meal']['id'] = 'field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['table'] = 'field_data_field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['field'] = 'field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['label'] = 'My Dream-About Meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['exclude'] = TRUE;
  /* Field: Content: Off-hours */
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['id'] = 'field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['table'] = 'field_data_field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['field'] = 'field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['label'] = 'What I Do When I\'m Not At Work';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['exclude'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[field_title]
[field_favorite_things]
[field_book_i_can_t_live_without]
[field_childhood_career_goal]
[field_favorite_moment_at_work]
[field_favorite_quote]
[field_favorite_vacation_spot]
[field_fun_fact]
[field_most_listened_to_song_on_m]
[field_my_dream_about_meal]
[field_what_i_do_when_i_m_not_at_]
';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_class'] = 'user-rollover';
  /* Sort criterion: Content: Sort Name (field_sort_name) */
  $handler->display->display_options['sorts']['field_sort_name_value']['id'] = 'field_sort_name_value';
  $handler->display->display_options['sorts']['field_sort_name_value']['table'] = 'field_data_field_sort_name';
  $handler->display->display_options['sorts']['field_sort_name_value']['field'] = 'field_sort_name_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'employee' => 'employee',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['row_class'] = 'user-[nid] employee active';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['label'] = 'Team List';
  $handler->display->display_options['header']['area']['content'] = '<ul><li class="team-filter">Filter:</li><li><a class="team-selector client_services" id="client_services"><span class="checkbox"></span>Client Services</a></li><li><a class="team-selector development" id="development"><span class="checkbox"></span>Development</a></li><li><a class="team-selector quality_assurance" id="quality_assurance"><span class="checkbox"></span>Quality Assurance</a></li><li><a class="team-selector user_experience" id="user_experience"><span class="checkbox"></span>User Experience</a></li><li><a class="team-selector marketing_services" id="marketing_services"><span class="checkbox"></span>Marketing Services</a></li></ul>
<select id="team-menu"><option class="team-selector client_services" id="client_services">Client Services</option><option class="team-selector development" id="development">Development</option><option class="team-selector marketing_services" id="marketing_services">Marketing Services</option><option class="team-selector quality_assurance" id="quality_assurance">Quality Assurance</option><option class="team-selector user_experience" id="user_experience">User Experience</option></select>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Alt Photo */
  $handler->display->display_options['fields']['field_alt_photo']['id'] = 'field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['table'] = 'field_data_field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['field'] = 'field_alt_photo';
  $handler->display->display_options['fields']['field_alt_photo']['label'] = '';
  $handler->display->display_options['fields']['field_alt_photo']['alter']['text'] = '<div class="employee-list-image" style="width:85px;height:85px;background-image:url(\'[field_alt_photo]\');"> </div>
<div class="employee-list-name">[title]</div>';
  $handler->display->display_options['fields']['field_alt_photo']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_alt_photo']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_alt_photo']['element_type'] = '0';
  $handler->display->display_options['fields']['field_alt_photo']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_alt_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_alt_photo']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_alt_photo']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_alt_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_alt_photo']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_alt_photo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = '0';
  $handler->display->display_options['fields']['title']['element_class'] = 'team-name';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Job Title */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['label'] = '';
  $handler->display->display_options['fields']['field_title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_title']['alter']['text'] = '[field_title]';
  $handler->display->display_options['fields']['field_title']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_title']['element_class'] = 'title';
  $handler->display->display_options['fields']['field_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_title']['element_default_classes'] = FALSE;
  /* Field: Content: A few favorite things */
  $handler->display->display_options['fields']['field_favorite_things']['id'] = 'field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['table'] = 'field_data_field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['field'] = 'field_favorite_things';
  $handler->display->display_options['fields']['field_favorite_things']['label'] = 'A Few of My Favorite Things';
  $handler->display->display_options['fields']['field_favorite_things']['exclude'] = TRUE;
  /* Field: Content: Book love */
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['id'] = 'field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['table'] = 'field_data_field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['field'] = 'field_book_i_can_t_live_without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['label'] = 'Book I Can\'t Live Without';
  $handler->display->display_options['fields']['field_book_i_can_t_live_without']['exclude'] = TRUE;
  /* Field: Content: Childhood aspiration */
  $handler->display->display_options['fields']['field_childhood_career_goal']['id'] = 'field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['table'] = 'field_data_field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['field'] = 'field_childhood_career_goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['label'] = 'Childhood Career Goal';
  $handler->display->display_options['fields']['field_childhood_career_goal']['exclude'] = TRUE;
  /* Field: Content: Favorite at-work moment */
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['id'] = 'field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['table'] = 'field_data_field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['field'] = 'field_favorite_moment_at_work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['label'] = 'Favorite Moment At Work';
  $handler->display->display_options['fields']['field_favorite_moment_at_work']['exclude'] = TRUE;
  /* Field: Content: Inspired quote */
  $handler->display->display_options['fields']['field_favorite_quote']['id'] = 'field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['table'] = 'field_data_field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['field'] = 'field_favorite_quote';
  $handler->display->display_options['fields']['field_favorite_quote']['label'] = 'Favorite Quote';
  $handler->display->display_options['fields']['field_favorite_quote']['exclude'] = TRUE;
  /* Field: Content: Choice vacation */
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['id'] = 'field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['table'] = 'field_data_field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['field'] = 'field_favorite_vacation_spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['label'] = 'Favorite Vacation Spot';
  $handler->display->display_options['fields']['field_favorite_vacation_spot']['exclude'] = TRUE;
  /* Field: Content: Surprise factor */
  $handler->display->display_options['fields']['field_fun_fact']['id'] = 'field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['table'] = 'field_data_field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['field'] = 'field_fun_fact';
  $handler->display->display_options['fields']['field_fun_fact']['label'] = 'Fun Fact About Me That Always Surprises People';
  $handler->display->display_options['fields']['field_fun_fact']['exclude'] = TRUE;
  /* Field: Content: Song addiction */
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['id'] = 'field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['table'] = 'field_data_field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['field'] = 'field_most_listened_to_song_on_m';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['label'] = 'Most Listened-to Song on My iPod';
  $handler->display->display_options['fields']['field_most_listened_to_song_on_m']['exclude'] = TRUE;
  /* Field: Content: Dream-about meal */
  $handler->display->display_options['fields']['field_my_dream_about_meal']['id'] = 'field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['table'] = 'field_data_field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['field'] = 'field_my_dream_about_meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['label'] = 'My Dream-About Meal';
  $handler->display->display_options['fields']['field_my_dream_about_meal']['exclude'] = TRUE;
  /* Field: Content: Off-hours */
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['id'] = 'field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['table'] = 'field_data_field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['field'] = 'field_what_i_do_when_i_m_not_at_';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['label'] = 'What I Do When I\'m Not At Work';
  $handler->display->display_options['fields']['field_what_i_do_when_i_m_not_at_']['exclude'] = TRUE;
  /* Field: Content: Specialities */
  $handler->display->display_options['fields']['field_specialities']['id'] = 'field_specialities';
  $handler->display->display_options['fields']['field_specialities']['table'] = 'field_data_field_specialities';
  $handler->display->display_options['fields']['field_specialities']['field'] = 'field_specialities';
  $handler->display->display_options['fields']['field_specialities']['exclude'] = TRUE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<ul id="fun-facts">
<li><span class="label">Specialties:</span>[field_specialities]</li>
</ul>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['element_wrapper_class'] = 'user-rollover';
  $handler->display->display_options['fields']['nothing']['hide_empty'] = TRUE;
  /* Field: Content: Team */
  $handler->display->display_options['fields']['field_employee_group']['id'] = 'field_employee_group';
  $handler->display->display_options['fields']['field_employee_group']['table'] = 'field_data_field_employee_group';
  $handler->display->display_options['fields']['field_employee_group']['field'] = 'field_employee_group';
  $handler->display->display_options['fields']['field_employee_group']['label'] = '';
  $handler->display->display_options['fields']['field_employee_group']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_employee_group']['alter']['text'] = '<div class="[field_employee_group-value] filter">[field_employee_group]</div>';
  $handler->display->display_options['fields']['field_employee_group']['element_type'] = '0';
  $handler->display->display_options['fields']['field_employee_group']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_employee_group']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_employee_group']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_employee_group']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_employee_group']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_employee_group']['separator'] = ' ';
  $handler->display->display_options['path'] = 'team';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Our Team';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $export['employee_list'] = $view;

  return $export;
}
