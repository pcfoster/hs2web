<?php
/**
 * @file
 * hs2_employees.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hs2_employees_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hs2_employees_node_info() {
  $items = array(
    'employee' => array(
      'name' => t('Employee'),
      'base' => 'node_content',
      'description' => t('Content type containing the characteristics fo an HS2 employee for display throughout the website'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
