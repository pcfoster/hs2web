# HS2 Website 

Redesign HS2 Website -- Drupal project, Acquia Cloud

## Documentation

- [JIRA home page](https://hs2studio.jira.com/projects/HS2WEB/summary)
- based on [Project Kickstarter](https://bitbucket.org/hs2studio/ac-vagrant-anisible)


## Team 

- **HS2 Project Manager:** [Ila Cipriano] (ila.cipriano@hs2solutions.com) 
- **HS2 Lead Designer:** [Lesley Guthrie] (lesley.guthrie@hs2solutions.com)
- **HS2 Lead Developer:** [Paul Foster] (paul.foster@hs2solutions.com)
- **HS2 Front End Lead:** [Sean Cairns] (sean.cairns@hs2solutions.com)

    
## Local Development Setup (update this section before pushing to new project repo)

1. Install Latest VirtualBox
2. Install Latest Vagrant
3. Fork the repo: https://bitbucket.org/hs2studio/HS2WEB/fork
4. Clone your fork:  
 `git clone git@bitbucket.org:YOURUSERNAME/HS2WEB.git`
5. Install Plugins  
 `cd HS2WEB/provisioning`  
 `vagrant plugin install vagrant-vbguest`  
 `vagrant plugin install vagrant-hostsupdater`
7. Startup Server (will take a bit)  
 `vagrant up`
8. SSH into VM and run the install profile script  
 `vagrant ssh`  
 `cd /var/www/html/docroot`  
 `drush si --db-url=mysql://root:root@localhost/HS2WEB --site-mail=YOUREMAIL@hs2solutions.com --account-name=admin --account-pass=admin`
9. View the site at http://HS2WEB.local
    

## Workflow

[Developer Workflow Documenation](https://docs.google.com/document/d/1d8K2VSmxnvrGNA4LhOrofTC0rrZ6-NtcO72JmmK4jXE/edit)

### Dev

<!-- <http://INSERT_DEV_URL> -->

Used primarily for basic integration. The `dev` branch is deployed here and automatically updated. NOTE: This environment has a limited database that may not accurately represent issues found in production.


### Stage

<!-- <http://INSERT_STAGE_URL> -->

Used for internal testing by the development team and UAT . The current release branch is deployed here and automatically updated.


### Prod

<!-- <http://INSERT_PROD_URL> -->

The public-facing, production environment.


## Directory Structure

The overall working structure is made up of a few directories. The following
explains each of their purposes and example contents.

- **bin**

    Currently this directory contains only the scripts required for the build process.

- **conf**

    Some docroot-level files must be substituted atop a base Drupal installation. The contents of this directory will overwrite any of these files. This folder can contain subdirectories. 
    *Examples: .htaccess, robots.txt*

- **docs**

    The overall documentation is included within this directory.

- **scripts**

    The current script types are `prebuild` and `postbuild`. These run before Drush make operations and after, respectively. Within each directory, any number of scripts is supported. As long as the file is executable, it can also be of any language. For example -

        scripts/
        ├── postbuild/
        │   └── post.sh
        └── prebuild/
            └── pre.sh

- **sites**

    Each individual site (other than all/) should be contained within this directory. As this is copied into place for the new build, any files in this directory will be moved to the build. The entire directory is copied to the build, so custom modules or additional configuration files can be included here. In addition, any *.make files included in the top-level of each site folder will be executed. For example -

        sites/
        ├── default
        │   ├── default.make
        │   └── settings.php
        ├── example.com
        │   ├── example.com.make
        │   └── settings.php
        └── example.net
            ├── example.net.make
            └── settings.php

- **tests**

    All test files should be placed into this directory.


## Testing Requirements

<!-- Customize this section to cover testing requirements; include details on automated code review processes, behat integration, unit testing, etc. -->

Each developer is expected to fully test their own code according to the following standards before submitting a pull request. 

    
## Deployment

<!-- Customize this section based on the client's deployment. Specifically, How Github and Acquia's repo are kept in sync.  -->


### Releases

Releases are managed by deploying tags to acquia clouds repo

### Hotfixes

<!-- Specify the process for creating and deploying hotfixes. -->
