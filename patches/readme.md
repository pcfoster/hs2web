# Patches

All Drupal patches should be placed in this directory. This ensures one consistent place for patches and avoids accidental patch deletion.
